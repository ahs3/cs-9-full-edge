#!/bin/bash
#
#	now that we've built a good source tree, send it to brew
#

#-- We assume we are in a git tree cloned from:
#
#	git@gitlab.com:ahs3/cs-9-full-edge.git
#
# and that ./setup.sh and ./build.sh have already been run
#

#-- what shall we call the localversion?
todays_date=$(date +"%Y%m%d")
full_version=".edge.$todays_date"
echo -e "\n==> Version: $full_version"
echo $full_version > localversion

#-- start in the right place
git checkout $todays_date

echo "==> BUILD_FLAGS=\"--arch-override=aarch64\" make dist-brew"
BUILD_FLAGS="--arch-override=aarch64" make dist-brew
