Why is this repo here?

   You can use this to create the latest RHEL for Edge kernel at any
   time.

To use this repo:

-- Clone it and change to the 'build' branch.

-- Run the setup script:

      $ ./setup.sh

-- Run the build script:

      $ ./build.sh

   This will create a branch 'yyyymmdd' with all known MRs and bug
   fixes; 'yyyymmdd' is always the current date.

-- Run the brew scipt:

      $ ./dobrew.sh

   This assumes the prior steps have been done and branch 'yyyymmdd'
   exists and is ready to go.  You can also do this by hand:

      $ git checkout yyyymmdd
      $ BUILD_FLAGS="--arch-override=aarch64" make dist-brew

-- Over time, it will be necessary to change the values in the array
   'mr_list' as MRs get merged, changed or added.

